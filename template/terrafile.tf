provider "digitalocean" {
  # You need to export this variable
  # export DIGITALOCEAN_TOKEN="Your API TOKEN"
  #
  # Please consider to use the Makefile to automate this. You can find on template folder
}

module "droplet" {
  source                  = "git@github.com:DNSFilter/terraform-digitalocean.git?ref=v0.6"
  name                    = "your_instance"
  droplet_count           = 1
  ssh_keys                = [26373294, 26392110]
  image                   = "ubuntu-18-04-x64"
  region                  = "nyc3"
  size                    = "8gb"
  rules                   = var.rules
  enable_firewall         = true
}

variable "rules" {
  description = "Inbound rules of DO firewall"
}

variable "rules_external" {
  description = "Inbound rules of DO firewall"
  default = []
}

output "ip_address" {
  value = module.droplet.ip_address
}