rules = [
 {
   protocol = "tcp"
   port_range = "22222"
   source_addresses = ["0.0.0.0/0", "::/0"]
   external   = false
 },
 {
   protocol = "tcp"
   port_range = "8080"
   source_addresses = ["0.0.0.0/0", "::/0"]
   external   = true
 },
]
