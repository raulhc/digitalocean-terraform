resource "digitalocean_droplet" "main" {
  for_each           = toset(var.hostnamelist)
  name               = each.value
  ssh_keys           = var.ssh_keys
  image              = var.image
  region             = var.region
  size               = var.size
  private_networking = var.private_networking
  backups            = var.backups
  ipv6               = var.ipv6
  user_data          = file("${path.module}/user_data.sh")
  tags               = ["app:${var.projectname}"]
}
