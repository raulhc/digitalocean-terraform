# Terraform module to create digitalocean resources.

This module:
* Creates droplet

## Requirements

To use the Makefile on **template folder** you will need:

 - [terraform](https://www.terraform.io/) >= 0.12
 - [jq](https://stedolan.github.io/jq)
 - [make](https://www.gnu.org/software/make/)

## Usage

Create a file **terrafile.tf** on the root of repository, you can follow this example:

```terraform
provider "digitalocean" {
  # You need to export this variable
  # export DIGITALOCEAN_TOKEN="Your API TOKEN"
  #
  # Please consider to use the Makefile to automate this. You can find on template folder
}


module "digitalocean" {
  source                  = "git@github.com:DNSFilter/terraform-digitalocean.git?ref=v0.4"
  name                    = "your_instance_name_here"
  # Obtain your ssh_key id number via your account. See Document https://developers.digitalocean.com/documentation/v2/#list-all-keys
  ssh_keys                = 26373294
  # To get the entire list: https://developers.digitalocean.com/documentation/v2/#list-all-images
  image                   = "ubuntu-18-04-x64"
  region                  = "nyc1"
  # To get the entire list: https://developers.digitalocean.com/documentation/v2/#sizes
  size                    = "8gb"
}

variable "rules" {
  description = "Inbound rules of DO firewall"
}

output "ip_address" {
  value = module.digitalocean.ip_address
}
```

Create a file **terraform.tfvars** on the root of repository, you can follow this example:

```
rules = [
 {
   protocol = "tcp"
   port_range = "22222"
   external   = false   # If it is false, you don't need to specify source, because it will get the list of droplet's ips
 },
 {
   protocol = "tcp"
   port_range = "8080"
   source_addresses = ["0.0.0.0/0", "::/0"]
   external   = true
 },
]
```

## Inputs

| **Name** | **Description** | **Type** | **Default** | **Required** |
|------|-------------|:----:|:-----:|:-----:|
| **hostnamelist** |  The list of droplet names. | list(string) | n/a | yes |
| **projectname** |  The project name. | string | n/a | yes |
| **ssh\_keys** | The ssh key, created on Digital Ocean to access the droplets. You must to pass a list (ex. [2221111, 2334563, 3194048] ) | string | n/a | yes |
| **image**| The image used to create the droplet. | string | n/a | yes |
| **region** | The digitalocean region to create things in. | string | n/a | yes |
| **size** | The size of droplet. | string | n/a | yes |
| **droplet_count** | Number of instances that will be created. | integer | `1` | no |
| **private_networking** | Option to create or not private_networking inside the same region. | boolean | true | no |
| **backups** | Option to setup or not periodic backup on droplet. | boolean | false | no |
| **ipv6** | Option to create or not ipv6 interface on droplet. | boolean | true | no |
| **priv_key_location** | The path for your private key. This key should be that private pair for \ **ssh_keys**.  | boolean | true | no |
| **ipv6** | Option to create or not ipv6 interface on droplet. | boolean | true | no |
| **enable_firewall** | Enable Digital Ocean firewall | boolean | false | no |

# ToDo

- [ ] Support ssh_keys creation
- [ ] Integrate with ansible 
- [ ] Add ssh key to be used by terraform cloud
- [ ] Use private network to communicate
- [x] Add droplet public or private ip on source_addresses
