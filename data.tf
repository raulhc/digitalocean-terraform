data "digitalocean_droplets" "external" {
  count = var.enable_external ? 1 : 0
  filter {
    key = "tags"
    values = ["external_"]
  }
}