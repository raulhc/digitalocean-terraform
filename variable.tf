variable "image" {
  description = "Image to create the instance"
}

variable "region" {
  description = "Region to create the droplet"
}

variable "size" {
  # To get the entire list: https://developers.digitalocean.com/documentation/v2/#sizes
  description = "Size of your droplet"
}

variable "ssh_keys" {
  # Obtain your ssh_key id number via your account. See Document https://developers.digitalocean.com/documentation/v2/#list-all-keys
  description = "List of ssh keys to use on your instance"
}

variable "droplet_count" {
  description = "Number of droplet to create"
  default = "1"
}

variable "private_networking" {
  description = "Enable private networking"
  default = true
}

variable "backups" {
  description = "Enable private networking"
  default = false
}

variable "ipv6" {
  description = "Enable private networking"
  default = true
}

variable "priv_key_location" {
  description = "The path for your private key"
  default     = "~/.ssh/id_rsa"
}

variable "projectname" {
  type        = string
  description = "The name of project"
}

variable "hostnamelist" {
  type        = list
  description = "The name of list of droplets"
}

variable "enable_firewall" {
  type        = bool
  default     = false
  description = "Enable DigitalOcean firewall usage"
}

variable "rules" {
  type = list(object({
    protocol = string
    port_range = number
    source_addresses = list(string)
    external   = bool
  }))
  description = "Inbound rules of DO firewall"
}

variable "enable_external" {
  type        = bool
  default     = true
  description = "Enable external Kafka servers to use the same firewall rules"
}