output "ip_address" {
  value = {
    for droplet in digitalocean_droplet.main:
    droplet.name => droplet.ipv4_address
  }
}