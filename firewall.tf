resource "digitalocean_firewall" "main" {
  count = var.enable_firewall ? 1 : 0
  name = "${var.projectname}-firewall"

  droplet_ids = [for droplet in digitalocean_droplet.main : droplet.id]

  dynamic "inbound_rule" {
    for_each = var.rules
    content {
      protocol = inbound_rule.value["protocol"]
      port_range = inbound_rule.value["port_range"]
      source_addresses = inbound_rule.value["external"] ? inbound_rule.value["source_addresses"] : var.private_networking ? [for droplet in digitalocean_droplet.main : droplet.ipv4_address_private] : [for droplet in digitalocean_droplet.main : droplet.ipv4_address]
    }
  }

  inbound_rule {
    protocol              = "tcp"
    port_range            = "2222"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "udp"
    port_range            = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }
}